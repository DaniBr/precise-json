import { defaultDatatypes, primitiveDatatypes } from "./defaultDatatypes"
import type { Datatype as DatatypeHandler } from "./newDatatype"
import type { ClassConstructor } from "./generalTypes"

export const reservedCodes = [
    ...Object.values(primitiveDatatypes).map(dt => dt.shortCode),
    ...Object.values(primitiveDatatypes).map(dt => dt.longName)
]

export type Index = {[K:string]:DatatypeHandler<any,any>}

/**
 * @internal
 */
export class datatypesDB {
    private readonly datatypes
    private readonly byKey
    private readonly byConstructor

    constructor (additionalDatatypes:DatatypeHandler<any,any>[], addDefaultTypes:boolean, key: keyof DatatypeHandler<any,any>) {
        this.datatypes = addDefaultTypes ? [...defaultDatatypes, ...additionalDatatypes] : additionalDatatypes
        this.byKey = createIndex(this.datatypes, key)
        this.byConstructor = createMap(this.datatypes)
    }

    getByKey (key: string):DatatypeHandler<any,any>|undefined {
        return this.byKey[key]
    }

    getByConstructor (constructor: ClassConstructor) { 
        return this.byConstructor.get(constructor)
    }
}

function createIndex (datatypes:DatatypeHandler<any,any>[], key: keyof DatatypeHandler<any,any>) {
    const result:Index = {}
    datatypes.forEach (datatype => {
        if (reservedCodes.includes(datatype[key]))
            throw new Error(`the ${key} "${datatype[key]}" is reserved word`)
        if (datatype[key] in result && !datatype.overwriteExisting)
            throw new Error (`"${datatype[key]}" can't overwrite other datatype with the same ${key} key`)
        result[datatype[key]] = datatype
    })
    return result as Index
}

function createMap (datatypes:DatatypeHandler<any,any>[]) {
    const result:Map<ClassConstructor, DatatypeHandler<any,any>> = new Map()
    datatypes.forEach(datatype => {
        //ToDo: callback function when legal overwriting occurs
        if (result.has(datatype.constructor) && !datatype.overwriteExisting)
            throw new Error (`"${datatype.constructor.name}" can't overwrite other datatype with the same constructor`)
        result.set (datatype.constructor, datatype)
    })
    return result
}