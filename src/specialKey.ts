const specialSymbol = '@' //must be just one character, also it's best to use a character that it illegal 
const escapeSymbol = '.' //must be just one character

/**
 * check if key of an object contain special type
 * i.e. if the key starts with '@'
 * example: {"@Date":"2022-11-15T20:02:43.728Z"}
 * @param key - represents key of an object
 * @internal
 */
export function isSpecialKey (key:string) {
    return key[0] == specialSymbol && key[1] != escapeSymbol
}

/**
 * escapes keys that starts with '@' by adding '@.'
 * example: {"@key":"value"} becomes {"@.@key":"value"}
 * @param key - represents key of an object
 * @internal
 */
export function escapeObjectKey (key:string) {
    if (key[0] == specialSymbol) return specialSymbol + escapeSymbol + key
    return key
}

/**
 * unescape keys that were escaped by escapeObjectKey function
 * example: {"@.@key":"value"} becomes {"@key":"value"}
 * @param key - represents key of an object
 * @internal
 */
export function unescapeObjectKey (key:string) {
    if (key[0]==specialSymbol && key[1]==escapeSymbol) return key.slice(2)
    return key
}

/**
 * adds '@' before the className or the shortCode
 * @param typeName - className or shortCode
 * @internal
 */
export function toSpecialKey (typeName:string) {
    return specialSymbol + typeName
}

/**
 * example: toSpecialObject("Regex","[A-z]+") returns {"@Regex":"[A-z]+"}
 * @param typeName - className or shortCode
 * @param value - any JSON supported value
 * @internal
 */
export function toSpecialObject <K extends string,V>(typeName:K, value:V) {
    return {[toSpecialKey(typeName)]:value}
}

/**
 * expects an object that may contain special key and 
 * return special key if it find one
 * @param obj an object that may contain special key
 * @returns key if special key exist, otherwise undefined
 * @internal
 */
export function getSpecialKeyIfExists (obj:Object) {
    const fullKey = Object.keys(obj).find( isSpecialKey )
    return fullKey?.slice(1)
}