import type { datatypeHandler } from "./newDatatype"

/**
 * Native JSON supported types
 * @public
 */
export type JsonNativeTypes =
    | string
    | number
    | boolean
    | null
    | JsonNativeTypes[]
    | { [k: string]: JsonNativeTypes }

/**
 * object which is a class constructor
 * @internal
 */
export type ClassConstructor = new (...args: any) => any

// don't know if we need this
/**
 * all types supported by PreciseJSON
 * @public
 */
export type PreciseJSONDefaultTypes =
    | JsonNativeTypes
    | String
    | Number
    | Boolean
    | Date
    | RegExp
    | Map<PreciseJSONDefaultTypes, PreciseJSONDefaultTypes>
    | Set<PreciseJSONDefaultTypes>
    | PreciseJSONDefaultTypes[]
    | { [k: string]: JsonNativeTypes }

/**
 * Options for PreciseJSON class
 * @public
 */
export interface PreciseJsonOptions {
    /**
     * When human readable is true the output JSON will be geared towards
     * developers i.e. the file will be more developers
     * when it false the JSON file be as optimal as possible
     */
    humanReadable?: boolean
    /**
     * when keepUndefined is false - PreciseJSON will keep native JSON behavior
     * that when a value is undefined the key and the value are removed
     * when keepUndefined is true PreciseJSON will make special value of
     * undefined when encountering one
     **/
    keepUndefined?: boolean
    /**
     * a numeric value to describe what would be the default amount of
     * indentation for the output JSON format text
     */
    indentation?: number
    /**
     * if loadDefaultTypes is set to true - PreciseJSON will add all the default
     * supported types of PreciseJSON @see PreciseJSONDefaultTypes
     * when false default types won't be loaded, user will have add his own types.
     */
    loadDefaultTypes?: boolean
    /**
     * an array of datatype handlers {@link datatypeHandler} to handled
     * custom types
     */
    customDatatypes?: ReturnType<typeof datatypeHandler>[]
}
