import type { PreciseJSON } from "./index"
import type { ClassConstructor, JsonNativeTypes } from "./generalTypes"


export type Datatype<MyType extends ClassConstructor, supportedValue extends JsonNativeTypes> = Readonly<{
    constructor: MyType
    longName: string
    shortCode: string
    overwriteExisting: boolean
    replacer: (originalValue: InstanceType<MyType>, helper: ReplacerHelper) => supportedValue
    reviver: (supportedValue: supportedValue, helper: ReviverHelper) => InstanceType<MyType>
}>

// same as Datatype, but just that some props are optional
export type NewDatatype<MyType extends ClassConstructor, supportedValue extends JsonNativeTypes> = Readonly<{
    constructor: MyType
    longName?: string
    shortCode?: string
    overwriteExisting?: boolean
    replacer: (originalValue: InstanceType<MyType>, helper: ReplacerHelper) => supportedValue
    reviver: (supportedValue: supportedValue, helper: ReviverHelper) => InstanceType<MyType>
}>

export type ReplacerHelper = Readonly<{
    isHumanReadable: boolean
    next: PreciseJSON["toJsonSupported"]
    constructor: ClassConstructor
}>

export type ReviverHelper = Readonly<{
    isHumanReadable: boolean
    next: PreciseJSON["toPreciseValue"]
    constructor: ClassConstructor
}>

/**
 * @param dt - datatype
 * @returns datatype handler
 * @public
 */
export function datatypeHandler<MyType extends ClassConstructor, supportedValue extends JsonNativeTypes> (dt:NewDatatype<MyType, supportedValue>) {
    const result:Datatype<MyType, supportedValue> = {
        constructor: dt.constructor,
        longName: dt.longName ?? dt.constructor.name,
        shortCode: dt.shortCode ?? dt.longName ?? dt.constructor.name,
        overwriteExisting: dt.overwriteExisting ?? false,
        replacer: dt.replacer,
        reviver: dt.reviver,
    }
    return result
}
