import type { PreciseJSON } from "./index"
import { escapeObjectKey, toSpecialObject } from "./specialKey"
import { primitiveDatatypes } from "./defaultDatatypes"
import type { ClassConstructor, JsonNativeTypes } from "./generalTypes"

/**
 * toJsonSupported take any value and turns into json supported
 * @param this - is the reference to PreciseJSON object 
 * @param value - any value
 * @returns json supported value
 * @internal
 */
export function toJsonSupported (this: PreciseJSON, value:unknown):JsonNativeTypes {
    switch (typeof value) {
        case "boolean":
        case "string":
            return value // Native JSON will deal with it
        case "number": {
            if (isFinite(value)) return value // Native JSON can deal with normal numbers
            // for special values, like NaN or Infinite
            const dt = primitiveDatatypes.number
            const key = this.humanReadable ? dt.longName : dt.shortCode
            return toSpecialObject (key, dt.replacer(value))
        }
        case "undefined": {
            const dt = primitiveDatatypes.undefined
            const key = this.humanReadable ? dt.longName : dt.shortCode
            return toSpecialObject (key, dt.replacer())
        }
        case "bigint": {
            const dt = primitiveDatatypes.bigint
            const key = this.humanReadable ? dt.longName : dt.shortCode
            return toSpecialObject (key, dt.replacer(value))
        }
        case "function":
            throw new Error("can't export functions, for security reasons")
        case "symbol":
            throw new Error("can't export symbols")
        case "object":
            if (value === null) return null // Native JSON will deal with it
            return objectHandler.call (this, value)
    }
}

function objectHandler (this: PreciseJSON, value:Object):JsonNativeTypes {
    const constructor = value.constructor as ClassConstructor
    if (constructor == Array)
        return continueArrayRecursion.call(this, value as Array<unknown>)
    if (constructor == Object)
        return continueObjectRecursion.call(this, value)

    // convert special types
    const datatypeHandler = this.index.getByConstructor(constructor)

    // note: this will not work on instances of customType
    if (datatypeHandler) {
        const key: string = this.humanReadable ?
            datatypeHandler.longName : datatypeHandler.shortCode
        const parsedValue = datatypeHandler.replacer (value, {
            isHumanReadable: this.humanReadable,
            next: item => toJsonSupported.call (this, item),
            constructor
        })
        return toSpecialObject(key, parsedValue)
    }

    // warn about non standard Object
    console.warn(`object of type ${constructor.name} is unescaped object`)
    // @ts-ignore
    if (typeof value.toJSON == "function") return value.toJSON()
    if (Array.isArray(value)) return continueArrayRecursion.call (this, value)
    return continueObjectRecursion.call (this, value)
}

function continueArrayRecursion (this: PreciseJSON, value:Array<any>) {
    return value.map(item => toJsonSupported.call(this, item))
}

function continueObjectRecursion (this: PreciseJSON, value:any) {
    const entries = Object.entries (value)
    if (!entries.length) return value
    return Object.fromEntries( entries.map( ([key,val]) => [escapeObjectKey(key), toJsonSupported.call(this, val)]))
}