import { getSpecialKeyIfExists, unescapeObjectKey, toSpecialKey } from "./specialKey"
import type { PreciseJSON } from "./index"
import type { JsonNativeTypes } from "./generalTypes"

/**
 * toPreciseValue takes a object that is JSON supported and returns original value
 * @param this - is the reference to PreciseJSON object 
 * @param value - JsonNative types 
 * @returns original precise value
 * @internal
 */
export function toPreciseValue (this: PreciseJSON, value:JsonNativeTypes):unknown {
    // Not a special value
    if (typeof value != "object") return value
    if (value === null) return null
    if (Array.isArray(value)) return continueArrayRecursion.call(this, value)
    const specialKey = getSpecialKeyIfExists (value)
    if (!specialKey) return continueObjectRecursion.call (this, value)
    const specialValue = value[toSpecialKey(specialKey)]
    
    // Special value
    switch (specialKey) {
        case "u":
        case "undefined":
            return undefined
        case "n":
        case "number":
            // @ts-ignore
            return Number (specialValue)
        case "bi":
        case "bigint":
            // @ts-ignore
            return BigInt (specialValue)
        default: {
            const datatypeHandler = this.index.getByKey (specialKey)
            if (!datatypeHandler) throw new Error(`got unsupported type ${specialKey}`)
            return datatypeHandler.reviver (specialValue, {
                isHumanReadable: this.humanReadable,
                next: item => toPreciseValue.call (this, item),
                constructor: datatypeHandler.constructor
            })
        }
    }
}

function continueArrayRecursion (this: PreciseJSON, value:Array<any>) {
    return value.map(item => toPreciseValue.call(this, item))
}

function continueObjectRecursion (this: PreciseJSON, value:Object&JsonNativeTypes) {
    const entries = Object.entries (value)
    if (!entries.length) return value
    return Object.fromEntries( entries.map( ([key,val]) => [unescapeObjectKey(key), toPreciseValue.call(this, val)]))
}