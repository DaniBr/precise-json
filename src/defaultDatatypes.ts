/// <reference lib="ES2020.BigInt" />
import { datatypeHandler } from "./newDatatype"

export const primitiveDatatypes = {
    number: {
        shortCode: 'n',
        longName: 'number',
        replacer: (n:number) => n.toString(),
        reviver: (n:string) => Number(n),
    },
    undefined: {
        longName: 'undefined',
        shortCode: 'u',
        replacer: () => 0,
        reviver: () => undefined,
    },
    bigint: {
        longName: 'bigint',
        shortCode: 'bi',
        replacer: (bi:bigint) => bi.toString(),
        reviver: (bi:string) => BigInt(bi),
    },
    // ToDo: add 'e' for empty slot in array
} as const

export const defaultDatatypes = [
    datatypeHandler ({
        constructor: Boolean,
        longName: "BooleanWrapper",
        shortCode: "B",
        replacer: (bool, {isHumanReadable}) => isHumanReadable ? bool.valueOf() : Number(bool),
        reviver: (bool) => new Boolean(bool),
    }),

    datatypeHandler ({
        constructor: Number,
        longName: "NumberWrapper",
        shortCode: "N",
        replacer: (num) => num.valueOf(),
        reviver: (num) => new Number(num),
        // ToDo: support infinite
    }),

    datatypeHandler ({
        constructor: String,
        longName: "StringWrapper",
        shortCode: "S",
        replacer: (str) => str.valueOf(),
        reviver: (str) => new String(str),
    }),

    datatypeHandler ({
        constructor: Date,
        shortCode: "D",
        replacer: (date, {isHumanReadable}) => isHumanReadable ? date.toJSON() : date.getTime(),
        reviver: (date) => new Date(date),
    }),

    datatypeHandler ({
        constructor: RegExp,
        shortCode: "R",
        replacer: (regex) => regex.flags ? [regex.source, regex.flags] : regex.source,
        reviver: (regex) => Array.isArray(regex) ? new RegExp(regex[0], regex[1]) : new RegExp(regex),
    }),

    datatypeHandler({
        constructor: Set,
        replacer: (set, {next}) => next([...set]),
        reviver: (arr, {next}) => new Set(next(arr) as unknown[]),
    }),

    datatypeHandler({
        constructor: Map,
        replacer: (map, {next}) => next([...map]),
        reviver: (arr, {next}) => new Map(next(arr) as [unknown,unknown][])
    }),

    /* TypedArrays */
    ...[
        {const: Int8Array, code: "I8"},
        {const: Uint8Array, code: "U8"},
        {const: Uint8ClampedArray, code: "U8C"},
        {const: Int16Array, code: "I16"},
        {const: Uint16Array, code: "U16"},
        {const: Int32Array, code: "I32"},
        {const: Uint32Array, code: "U32"},
        {const: Float32Array, code: "F32"},
        {const: Float64Array, code: "F64"},
    ].map ( t => datatypeHandler({
        constructor: t.const,
        shortCode: t.code,
        replacer: typedArray => Array.from (typedArray),
        reviver: array => new t.const (array)
    })),
    
    /* BigIntArray (for supported browsers) */
    ... typeof BigInt == "function" ? [
        {const: BigInt64Array, code: "I64"},
        {const: BigUint64Array, code: "U64"},
    ].map ( t => datatypeHandler({
        constructor: t.const,
        shortCode: t.code,
        replacer: typedArray => Array.from(typedArray).map( n => n.toString()),
        reviver: array => new t.const (array.map( s => BigInt(s) ))
    })) : [/* empty, adding nothing */],

] as const
