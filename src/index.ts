import { toJsonSupported } from "./toJsonSupported"
import { toPreciseValue } from "./toPreciseValue"
import { datatypesDB } from "./datatypesDB"
import type { datatypeHandler } from "./newDatatype"
import type { JsonNativeTypes } from "./generalTypes"

/**
 * superset of JSON that supports many types from the JavaScript standard library.
 * @public
 */
export class PreciseJSON {
    /**
     * Indicates whether the JSON will be human-readable.
     * example of human readable: `{"@Date":"2022-12-06T19:39:28.266Z"}`
     * example of non human readable: `{"@D":1670355568266}`.
     */
    public readonly humanReadable: boolean

    /**
     * The number of spaces for indentation in the JSON.
     */
    public readonly indentation: number

    /**
     * Indicates whether undefined values are preserved when generating the JSON.
     */
    public readonly keepUndefined: boolean

    /**
     * Indicates whether the default datatypes were loaded.
     */
    public readonly loadDefaultTypes: boolean

    /**
     * The datatypes supported by this `PreciseJSON` instance, indexed by constructor and by name (short/long).
     */
    public readonly index: datatypesDB

    /**
     * Creates a new `PreciseJSON` instance.
     *
     * @param options An object with the following optional properties:
     * - `humanReadable`:  Indicates whether the JSON should be human-readable.
     * - `indentation`: The number of spaces for indentation in the JSON.
     * - `keepUndefined`: Indicates whether undefined values should be preserved when generating the JSON.
     * - `loadDefaultTypes`: Indicates whether the default datatypes should be loaded to the new `PreciseJSON` instance.
     * - `customDatatypes`: An array of custom {@link datatypeHandler | datatype handlers} to add to the new `PreciseJSON` instance.
     */
    constructor(
        options: {
            humanReadable?: boolean
            keepUndefined?: boolean
            indentation?: number
            loadDefaultTypes?: boolean
            customDatatypes?: ReturnType<typeof datatypeHandler>[]
        } = {}
    ) {
        options = options || {}
        this.humanReadable = options.humanReadable ?? false
        this.keepUndefined = options.keepUndefined ?? true
        this.indentation = options.indentation ?? 0
        this.loadDefaultTypes = options.loadDefaultTypes ?? true
        const keyFrom = 0
        this.index = new datatypesDB(
            options.customDatatypes ?? [],
            this.loadDefaultTypes,
            this.humanReadable ? "longName" : "shortCode"
        )
    }

    /**
     * a method that takes any value that is supported by
     * the newly created PreciseJSON instance
     * (and perhaps not supported by JSON) and turns it into
     * native JSON supported types {@link JsonNativeTypes}
     * that later on can be converted back into the original value
     * @param value - any type that is supported by PreciseJSON
     * @returns JSON supported value
     */
    toJsonSupported(value: unknown) {
        return toJsonSupported.call(this, value)
        // ToDo: shorter style i.e. toJsonSupported = toJsonSupported
    }

    /**
     * method that converts value from PreciseJSON to the original value
     * the way it was before it was stringify by PreciseJSON
     * @param value - returned value from toJsonSupported
     * @returns the original precise value
     */
    toPreciseValue(value: JsonNativeTypes) {
        return toPreciseValue.call(this, value)
    }

    /**
     * method to convert any supported PreciseJSON value and returns a JSON format string
     * @param value - any value that is supported by PreciseJSON
     * @returns JSON format string
     */
    stringify(value: any) {
        const jsonSupportedValue = this.toJsonSupported(value)
        return JSON.stringify(jsonSupportedValue)
    }

    /**
     * PreciseJSON parser, this method expect a JSON format string
     * and will covert any value that was serialized by PreciseJSON.stringify
     * to it's original value
     * @param text - json format string
     * @returns original precise value
     */
    parse(text: string) {
        const jsonSupportedValue = JSON.parse(text)
        return this.toPreciseValue(jsonSupportedValue)
    }

    /**
     * static method that takes any value that is supported by
     * PreciseJSON default supported types {@link PreciseJSONDefaultTypes}
     * (and perhaps not supported by JSON) and turns it into
     * native JSON supported types {@link JsonNativeTypes}
     * that later on can be converted back into the original value
     * @param value - any type that is supported by PreciseJSON
     * @returns JSON supported value
     */
    static toJsonSupported(value: any) {
        return defaultPreciseJSON.toJsonSupported(value)
    }
    /**
     * static method that takes a object that was returned by
     * {@link PreciseJSON.toJsonSupported} and turns it
     * to the original value
     * @param value - returned value from toJsonSupported
     * @returns the original precise value
     */
    static toPreciseValue(value: any /*JsonNativeTypes*/) {
        return defaultPreciseJSON.toPreciseValue(value)
    }

    /**
     * a static method that convert any supported PreciseJSON default types
     * {@link PreciseJSONDefaultTypes} into JSON format string
     * @param value - any value that is supported by PreciseJSON
     * @returns JSON format string
     */
    static stringify(value: any) {
        return defaultPreciseJSON.stringify(value)
    }

    /**
     * static PreciseJSON parser, this method expect a JSON format string
     * and will covert any value that was serialized by PreciseJSON.stringify
     * to it's original value
     * @param text - json format string
     * @returns original precise value
     */
    static parse(text: string) {
        return defaultPreciseJSON.parse(text)
    }
    // ToDo?: static checkSupport()
}

const defaultPreciseJSON = new PreciseJSON()

export { toJsonSupported } from "./toJsonSupported"
export { toPreciseValue } from "./toPreciseValue"
export { datatypesDB } from "./datatypesDB"
export type { datatypeHandler } from "./newDatatype"
export type {
    JsonNativeTypes,
    PreciseJsonOptions,
    PreciseJSONDefaultTypes
} from "./generalTypes"
