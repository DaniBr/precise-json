# PreciseJSON: A JavaScript Library for Extensive JSON Support
## Overview

**PreciseJSON** provides an extensive version of `JSON`. you can use **PreciseJSON** to serialize and deserialize almost all types in the JavaScript standard library, except functions and symbols. without any loss of information or modification to the original data. It offers a similar API to the native `JSON` and enables the creation of new instances that support custom types. This makes it easier to work with complex data structures, without worrying about data loss or inconsistency during the serialization/deserialization process.

## Basic Examples

### working with bigint

JSON doesn't support bigint values. If you use the native JSON API to stringify a bigint, you'll get a TypeError saying that "JSON does not support bigints." `PreciseJSON` support bigint out of the box

```js
const myObject = { value: BigInt(9007199254740991) }
const nativeJSON = JSON.stringify(myObject) // Throws TypeError: Do not know how to serialize a BigInt

const preciseJSON = PreciseJSON.stringify(myObject)
console.log(preciseJSON) // '{"value": {"@BigInt": "9007199254740991"}}'

const preciseParse = PreciseJSON.parse(preciseJSON)
console.log(preciseParse.value instanceof BigInt) // true
```

### working with Date

native JSON converts dates into strings wheres PreciseJSON supports dates natively, This can be very useful, as it eliminates the need to convert dates after receiving them in JSON.

```js
import PreciseJSON from "precise-json"

const myObject = { name: "Dan", DOB: new Date("1999-04-25") }

//using native JSON
const json = JSON.stringify(myObject)
const parsed = JSON.parse()
console.log(parsed.DOB instanceof Date) // false
console.log(new Date(parsed.DOB) instanceof Date) // true

//using PreciseJSON
const preciseJSON = PreciseJSON.stringify(myObject)
console.log(preciseJSON) // '{"name":"Dan","DOB": {"@Date": "1999-04-25T00:00:00.000Z"}}'

const preciseParse = PreciseJSON.parse(preciseJSON)
console.log(preciseParse.DOB instanceof Date) // true
```

## Comparing JSON to PreciseJSON

We've provided two examples that demonstrate how handy PreciseJSON can be. However, PreciseJSON supports a variety of values. Here is a full list of the values supported by PreciseJSON, along with a comparison of how JSON handles them.

| Data Type         | Example Input                    | JSON.stringify Output             | PreciseJSON Handling                            |
| ----------------- | -------------------------------- | --------------------------------- | ----------------------------------------------- |
| string            | `"Hello"`                        | `"Hello"`                         | Same, string is already valid JSON              |
| number            | `1000`                           | `1000`                            | Same, number is already valid JSON              |
| Boolean           | `true`                           | `true`                            | Same, number is already valid JSON              |
| Null              | `null`                           | `null`                            | Same, null is already valid JSON                |
| Array             | `[1, 2, 3]`                      | `[1, 2, 3]`                       | Same, array is already valid JSON \*\*          |
| Object literal    | `{ key:"value" }`                | `{ "key":"value" }`               | Same, object literal is already valid JSON \*\* |
| String Wrapper    | `new String("Hello")`            | `"Hello"`                         | `{"@String": "Hello"}`                          |
| Number Wrapper    | `new Number(1000)`               | `1000`                            | `{"@Number": 1000}`                             |
| Boolean Wrapper   | `new Boolean(true)`              | `true`                            | `{"@Boolean": true}`                            |
| BigInt            | `new Bigint("9007199254740991")` | Throws `TypeError`                | `{"@BigInt": "9007199254740991"}`               |
| Date              | `new Date("2022-04-01")`         | `"2022-04-01T00:00:00.000Z"`      | `{"@Date": "2022-0401T00:00:00.000Z"}`          |
| Set               | `new Set(["hello", "world"])`    | `{}`                              | `{"@Set": ["hello", "world"]}`                  |
| Map               | `new Map([["key", "value"]])`    | `{}`                              | `{"@Map": [["key", "value"]]}`                  |
| undefined         | `undefined`                      | `JSON` deletes the key/value pair | `{"@undefined": 0}`                             |
| [] with undefined | `[1, undefined]`                 | `[1, null]`                       | `[1, {"@undefined": 0}]`                        |
| Infinity          | `Number.MAX_VALUE * 2`           | `"null"`                          | `{"@Number": "Infinity}`                        |
| -Infinity         | `-Number.MAX_VALUE * 2`          | `"null"`                          | `{"@Number": "-Infinity}`                       |
| NaN               | `NaN * NaN`                      | `"null"`                          | `{"@Number": "NaN}`                             |

\*\* in case where all the values are supported by JSON

> **_NOTE:_** The examples of PreciseJSON output provided in this table, assumes that the mode of `PreciseJSON` is set to humanreadable, if humanreadable is set to false, the output may differ, for instance `new Date("1970-01-01")` will be serialized as `{"@D": 0}` \( where 0 is the equivalent of `Date.getTime()`\) instead of `{"@Date": "1970-01-01T00:00:00.000Z"}`

## Customizing PreciseJSON

PreciseJSON offers a range of customization options for serializing and deserializing data. Here are a few of the ways you can customize PreciseJSON:

-   You can choose whether the resulting JSON string should be human-readable or compact.
-   You can specify the number of spaces used for indentation in the JSON.
-   You can decide whether undefined values should be preserved when generating the JSON. (For more information, see this. [ link](https://petermekhaeil.com/how-to-keep-undefined-values-in-json.stringify/))
-   You can choose whether default datatypes should be loaded into the new PreciseJSON instance.
-   Add custom types

###

This can be useful if you need to support data types that are not natively supported by PreciseJSON, or if you want to control the exact format of the serialized data.

To customize PreciseJSON, you can

The idea behind **PreciseJSON** is _Precision_, With **PreciseJSON**, you can always be confident that the data you send and receive over the wire using JSON format, is always accurate."

## Installation

To use `PreciseJSON,` you can install it from npm using the following command:

```bash
`npm install precise-json`
```
