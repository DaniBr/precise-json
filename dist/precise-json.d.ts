/**
 * object which is a class constructor
 * @internal
 */
declare type ClassConstructor = new (...args: any) => any;

declare type Datatype<MyType extends ClassConstructor, supportedValue extends JsonNativeTypes> = Readonly<{
    constructor: MyType;
    longName: string;
    shortCode: string;
    overwriteExisting: boolean;
    replacer: (originalValue: InstanceType<MyType>, helper: ReplacerHelper) => supportedValue;
    reviver: (supportedValue: supportedValue, helper: ReviverHelper) => InstanceType<MyType>;
}>;

/**
 * @param dt - datatype
 * @returns datatype handler
 * @public
 */
export declare function datatypeHandler<MyType extends ClassConstructor, supportedValue extends JsonNativeTypes>(dt: NewDatatype<MyType, supportedValue>): Readonly<{
    constructor: MyType;
    longName: string;
    shortCode: string;
    overwriteExisting: boolean;
    replacer: (originalValue: InstanceType<MyType>, helper: Readonly<{
        isHumanReadable: boolean;
        next: (value: any) => JsonNativeTypes;
        constructor: ClassConstructor;
    }>) => supportedValue;
    reviver: (supportedValue: supportedValue, helper: Readonly<{
        isHumanReadable: boolean;
        next: (value: JsonNativeTypes) => unknown;
        constructor: ClassConstructor;
    }>) => InstanceType<MyType>;
}>;

/**
 * @internal
 */
export declare class datatypesDB {
    private readonly datatypes;
    private readonly byKey;
    private readonly byConstructor;
    constructor(additionalDatatypes: Datatype<any, any>[], addDefaultTypes: boolean, key: keyof Datatype<any, any>);
    getByKey(key: string): Datatype<any, any> | undefined;
    getByConstructor(constructor: ClassConstructor): Readonly<{
        constructor: any;
        longName: string;
        shortCode: string;
        overwriteExisting: boolean;
        replacer: (originalValue: any, helper: Readonly<{
            isHumanReadable: boolean;
            next: (value: any) => JsonNativeTypes;
            constructor: ClassConstructor;
        }>) => any;
        reviver: (supportedValue: any, helper: Readonly<{
            isHumanReadable: boolean;
            next: (value: JsonNativeTypes) => unknown;
            constructor: ClassConstructor;
        }>) => any;
    }> | undefined;
}

/**
 * Native JSON supported types
 * @public
 */
export declare type JsonNativeTypes = string | number | boolean | null | JsonNativeTypes[] | {
    [k: string]: JsonNativeTypes;
};

declare type NewDatatype<MyType extends ClassConstructor, supportedValue extends JsonNativeTypes> = Readonly<{
    constructor: MyType;
    longName?: string;
    shortCode?: string;
    overwriteExisting?: boolean;
    replacer: (originalValue: InstanceType<MyType>, helper: ReplacerHelper) => supportedValue;
    reviver: (supportedValue: supportedValue, helper: ReviverHelper) => InstanceType<MyType>;
}>;

/**
 * PreciseJSON is an extensive version of JSON that support almost all the types
 * that are in the JavaScript standard library (excluding function and Symbol)
 * The main entry point to PreciseJSON is the {@link PreciseJSON} class,
 * which returns a class that is very similar to the api of the JSON
 * defined in the standard library
 * with the ability to add support for custom types
 * @public
 */
export declare class PreciseJSON {
    readonly humanReadable: boolean;
    readonly indentation: number;
    readonly keepUndefined: boolean;
    readonly loadDefaultTypes: boolean;
    readonly index: datatypesDB;
    /**
     * a constructor to make a new instance of PreciseJSON with custom options
     * @param options - what are the options {@link PreciseJsonOptions}
     */
    constructor(options?: PreciseJsonOptions);
    /**
     * convert any value (optimally supported by PreciseJSON)
     * into native JSON supported types {@link JsonNativeTypes}
     * that can later on be converted back into it's original value
     * via toPreciseValue \{\@link PreciseJSON.(toPreciseValue:static)\},
     * any non supported value i.e. function symbol or custom type
     * that was not added to PreciseJSON constructor,
     * will throw an error or a warning
     * @param value - any value (optimally supported by PreciseJSON)
     * @returns JSON supported value
     */
    toJsonSupported(value: any): JsonNativeTypes;
    /**
     * convert any value into a JSON format string ,that can later
     * on be restored into it's original value via PreciseJSON.parse method
     * \{\@link (parse:instance)\}, any non supported value i.e. function symbol
     * or custom type that was not added to PreciseJSON constructor,
     * will throw an error or a warning
     * @param value - any value that is supported by PreciseJSON
     * @returns JSON format string
     */
    stringify(value: any): string;
    /**
     * convert an object that was returned by
     * toJsonSupported \{\@link (toJsonSupported:instance)\}
     * and turns it into it's original value
     * @param value - returned value from toJsonSupported
     * @returns the original precise value
     * {@label toPrecise}
     */
    toPreciseValue(value: JsonNativeTypes): unknown;
    /**
     * expects "JSON format" string, parses it into an object,
     * if it finds any special value withing the parsed Object
     * (identified with keys thats starts with \@)
     * it will convert it back to it's original value
     * @param text - "JSON format" string
     * @returns the original precise value
     */
    parse(text: string): unknown;
    /**
     * static method that takes any value that is supported by
     * PreciseJSON default supported types {@link PreciseJSONDefaultTypes}
     * (will warn about any non supported type)
     * and perhaps not supported by JSON and turns it into
     * native JSON supported types {@link JsonNativeTypes}
     * that later on can be converted back into it's original value
     * @param value - any type that is supported by PreciseJSON
     * @returns JSON supported value
     */
    static toJsonSupported(value: any): JsonNativeTypes;
    /**
     * static method that takes a object that was returned by
     * \{\@link PreciseJSON.(toJsonSupported:static)\}
     * and turns it into the original value
     * @param value - returned value from toJsonSupported
     * @returns the original precise value
     */
    static toPreciseValue(value: any): unknown;
    /**
     * a static method that convert any supported PreciseJSON default types
     * {@link PreciseJSONDefaultTypes} into JSON format string
     * @param value - any value that is supported by PreciseJSON
     * @returns JSON format string
     */
    static stringify(value: any): string;
    /**
     * static PreciseJSON parser, this method expect "JSON format" string
     * parses it into an object, if it finds any special value
     * withing the parsed Object i.e. keys thats starts with \@
     * it will convert it back to it's original value
     * @param text - json format string
     * @returns original precise value
     */
    static parse(text: string): unknown;
    /**
     * PreciseJSON uses some latest and greatest JavaScript features
     * we try to be as backward compatible as possible (ES6), but never the less
     * this method will return rather or not the JavaScript environment support
     * the various language features we used
     * @returns rather or not the JavaScript environment support PreciseJSON
     */
    static isSupported(): boolean;
}

/**
 * all types supported by PreciseJSON
 * @public
 */
export declare type PreciseJSONDefaultTypes = JsonNativeTypes | String | Number | Boolean | Date | RegExp | Map<PreciseJSONDefaultTypes, PreciseJSONDefaultTypes> | Set<PreciseJSONDefaultTypes> | PreciseJSONDefaultTypes[] | {
    [k: string]: JsonNativeTypes;
};

/**
 * Options for PreciseJSON class
 * @public
 */
export declare interface PreciseJsonOptions {
    /**
     * When human readable is true the output JSON will be geared towards
     * developers i.e. the file will be more developers
     * when it false the JSON file be as optimal as possible
     */
    humanReadable?: boolean;
    /**
     * when keepUndefined is false - PreciseJSON will keep native JSON behavior
     * that when a value is undefined the key and the value are removed
     * when keepUndefined is true PreciseJSON will make special value of
     * undefined when encountering one
     **/
    keepUndefined?: boolean;
    /**
     * a numeric value to describe what would be the default amount of
     * indentation for the output JSON format text
     */
    indentation?: number;
    /**
     * if loadDefaultTypes is set to true - PreciseJSON will add all the default
     * supported types of PreciseJSON @see PreciseJSONDefaultTypes
     * when false default types won't be loaded, user will have add his own types.
     */
    loadDefaultTypes?: boolean;
    /**
     * an array of datatype handlers {@link datatypeHandler} to handled
     * custom types
     */
    customDatatypes?: ReturnType<typeof datatypeHandler>[];
}

declare type ReplacerHelper = Readonly<{
    isHumanReadable: boolean;
    next: PreciseJSON["toJsonSupported"];
    constructor: ClassConstructor;
}>;

declare type ReviverHelper = Readonly<{
    isHumanReadable: boolean;
    next: PreciseJSON["toPreciseValue"];
    constructor: ClassConstructor;
}>;

/**
 * toJsonSupported take any value and turns into json supported
 * @param this - is the reference to PreciseJSON object
 * @param value - any value
 * @returns json supported value
 * @internal
 */
export declare function toJsonSupported(this: PreciseJSON, value: unknown): JsonNativeTypes;

/**
 * toPreciseValue takes a object that is JSON supported and returns original value
 * @param this - is the reference to PreciseJSON object
 * @param value - JsonNative types
 * @returns original precise value
 * @internal
 */
export declare function toPreciseValue(this: PreciseJSON, value: JsonNativeTypes): unknown;

export { }
