import { expect } from "chai"
import { PreciseJSON } from "../src"

function stringifyAndParse(data: any) {
    return PreciseJSON.parse(PreciseJSON.stringify(data))
}
describe("PreciseJSON", () => {
    describe("Support for native JSON values", () => {
        it("simple", () => {
            const data = [true, 2, "3", [[4]], { n: 5 }]
            expect(stringifyAndParse(data)).to.eql(data)
        })
        it("negative or empty", () => {
            const data = [false, 0, "", null, [], {}]
            expect(stringifyAndParse(data)).to.eql(data)
        })
    })
    describe("Empty values", () => {
        it("undefined as item of an array or property of an object", () => {
            const data = [undefined, { key: undefined }]
            expect(stringifyAndParse(data)).to.eql(data)
        })
    })

    describe("special numbers", () => {
        it("NaN", () => {
            const data = NaN
            expect(stringifyAndParse(data)).to.eql(data)
        })
        it("Infinity", () => {
            const data = [Infinity, -Infinity]
            expect(stringifyAndParse(data)).to.eql(data)
        })

        // when BigInt Supported
        if (typeof BigInt === "function") {
            it("BigInt", () => {
                const data = BigInt("100000000000000001")
                expect(stringifyAndParse(data)).to.eql(data)
            })

            it("BigInt array", () => {
                const data = new BigUint64Array([BigInt("100000000000000001")])
                expect(stringifyAndParse(data)).to.eql(data)
            })
        }
    })

    describe("General behavior", () => {
        it("should handle objects with '@' in property name", () => {
            const data = [{ "@n": "1" }, { "@@t@": 1 }]
            expect(stringifyAndParse(data)).to.eql(data)
        })

        it("PreciseJSON.stringify should be a pure function, meaning it doesn't change the object it stringifies", () => {
            const obj1 = [false, { a: NaN, b: { c: Infinity }, "@n": "14", $n: 0 }, /[0-9]/, undefined]
            const obj2 = [false, { a: NaN, b: { c: Infinity }, "@n": "14", $n: 0 }, /[0-9]/, undefined]
            PreciseJSON.stringify(obj1)
            expect(obj1).to.eql(obj2)
        })
    })

    describe("Support for Date & RegExp", () => {
        it("should handle Date objects", () => {
            const data = new Date("2022-01-23T20:54:56Z")
            expect(stringifyAndParse(data)).to.eql(data)
        })
        it("should handle simple RegExp objects", () => {
            const data = new RegExp("[A-z0-9]*")
            expect(stringifyAndParse(data)).to.eql(data)
        })

        it("should handle RegExp objects with flags", () => {
            const data = new RegExp("[A-z0-9]*", "g")
            expect(stringifyAndParse(data)).to.eql(data)
        })
    })

    describe("Support for TypedArray objects", () => {
        it("should handle Int16Array", () => {
            const data = new Int16Array([12345, 6789])
            expect(stringifyAndParse(data)).to.eql(data)
        })

        it("should handle Float32Array", () => {
            const data = new Float32Array([3.1415, 0])
            expect(stringifyAndParse(data)).to.eql(data)
        })
    })

    describe("Support for Set & Map objects", () => {
        it("should handle simple Set objects", () => {
            const data = new Set([1, 1])
            expect(stringifyAndParse(data)).to.eql(data)
        })

        it("should handle nested Set objects", () => {
            const data = new Set([1, new Set([2, [3, 3]])])
            expect(stringifyAndParse(data)).to.eql(data)
        })

        it("Simple Map", () => {
            const data = new Map<any, any>([
                [1, 2],
                ["3", "4"]
            ])
            expect(stringifyAndParse(data)).to.eql(data)
        })

        it("Nested Map as key and value", () => {
            const keyMap = new Map([[1, 2]])
            const valueMap = new Map([[1, 2]])

            const data = new Map<any, any>([
                [1, 2],
                [keyMap, valueMap]
            ])
            expect(stringifyAndParse(data)).to.eql(data)
        })
    })
})
