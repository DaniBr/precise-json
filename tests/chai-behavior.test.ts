import { expect } from "chai"

/*
Testing how Chai's `eql` handles tricky types, in order to make ensure that `Chai` remains consistent in the futures versions.
*/
describe("Mocha-Chai behavior", () => {
    describe("Special numbers", () => {
        it("(new Number) eql (new Number)", () => {
            expect(Infinity).to.not.eql(-Infinity)
        })

        it("(new Number) eql (new Number)", () => {
            expect(NaN).to.eql(NaN)
        })
    })

    describe("Empty values", () => {
        it("(null) not eql (undefined", () => {
            expect(null).to.not.eql(undefined)
        })
        it("(undefined) not eql (empty slot)", () => {
            expect([undefined]).to.not.eql([,])
        })
    })

    describe("Wrapper for primitive", () => {
        it("(new Number) eql (new Number)", () => {
            expect(new Number(8)).to.eql(new Number(8))
        })

        it("(new Number) not eql (number)", () => {
            expect(new Number(8)).to.not.eql(8)
        })

        it("(new Number) not equal (new Number)", () => {
            expect(new Number(8)).to.not.equal(new Number(8))
        })
    })

    it("(ExtArray) eql (Array)", () => {
        class ExtArray extends Array {
            constructor(...i: any) {
                super(...i)
            }
            public get lastIndex() {
                return this.length - 1
            }
        }
        expect(new ExtArray(1, 2, 3)).to.eql(new Array(1, 2, 3))
    })

    describe("Date & Regex", () => {
        it("(regex with flag) eql (regex with flag)", () => {
            let a = /[A-z0-9]*/gi
            let b = new RegExp("[A-z0-9]*", "gi")
            expect(a).to.eql(b)
        })
        it("(regex with flag) not eql (regex without flag)", () => {
            let a = /[A-z0-9]*/gi
            let b = new RegExp("[A-z0-9]*")
            expect(a).to.not.eql(b)
        })
        it("(regex) not eql (string)", () => {
            let a = /[A-z0-9]*/
            let b = "[A-z0-9]*"
            expect(a).to.not.eql(b)
        })
        it("same date", () => {
            let a = new Date("2022-01-01T20:00:00Z")
            let b = new Date("2022-01-01T20:00:00Z")
            expect(a).to.eql(b)
        })
        it("different date (just by few milliseconds)", () => {
            let a = new Date("2022-01-01T20:00:00Z")
            let b = new Date("2022-01-01T20:00:00.245Z")
            expect(a).to.not.eql(b)
        })
    })

    describe("Map & Set", () => {
        it("plain Set comparison", () => {
            let a = new Set([2, 2, 1])
            let b = new Set([1, 1, 1, 1, 2])
            expect(a).to.eql(b)
        })
        it("plain Map comparison", () => {
            let a = new Map([[1, 2]])
            let b = new Map([[1, 2]])
            expect(a).to.eql(b)
        })
        it("nested Set comparison", () => {
            let a = new Set([1, new Set([2, 2, 2, 2])])
            let b = new Set([1, new Set([2, 2, 2, 2])])
            expect(a).to.eql(b)
        })
        it("nested Set comparison (unequal)", () => {
            let a = new Set([1, new Set([2, 2, 2, 2])])
            let b = new Set([1, 1, new Set([2, 3])])
            expect(a).to.not.eql(b)
        })
        it("nested Map comparison", () => {
            let a = new Map([[new Map([[1, 1]]), new Map([[1, 1]])]])
            let b = new Map([[new Map([[1, 1]]), new Map([[1, 1]])]])
            expect(a).to.eql(b)
        })
        it("nested Map comparison (unequal)", () => {
            let a = new Map([[new Map([[1, 222]]), new Map([[1, 1]])]])
            let b = new Map([[new Map([[1, 111]]), new Map([[1, 1]])]])
            expect(a).to.not.eql(b)
        })
    })
})
